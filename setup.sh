# get rid of old php
sudo yum -y remove php56 php56-cli php56-common php56-jsonc php56-mysqlnd php56-pdo php56-pecl-xdebug php56-process php56-xml

# get rid of old mysql
sudo yum -y remove mysql55 mysql55-libs mysql55-server php56-mysqlnd

# install new mysql
sudo yum -y install mysql57 mysql57-server

# install new php
sudo yum -y install php73 php73-mysqlnd php73-mbstring php73-gd

# install SSL
sudo yum install mod24_ssl

# install phpMyAdmin

# fixup permissions and add directories
chmod +x /home/ec2-user
mkdir -p /home/ec2-user/environment/www/html


# fixup httpd.conf
sudo sed -i '/DocumentRoot/s/\/var\//\/home\/ec2-user\/environment\//' /etc/httpd/conf/httpd.conf
sudo sed -i 's/Listen 80/Listen 8080/' /etc/httpd/conf/httpd.conf
sudo cp 000cpsc2030.conf /etc/httpd/conf.d


# create new index page
cat <<EOF >/home/ec2-user/environment/www/html/index.html
<!DOCTYPE html>
<html>
    <head>
        <title>AWS Cloud9 CPSC 2030</title>
    </head>
    <body>
        <h1>AWS Cloud9 CPSC 2030</h1>
        <p><a href="phpMyAdmin/">phpMyAdmin</a></p>
    </body>
</html>
EOF

cd /var/www/html
wget -O /tmp/phpMyAdmin.tar.gz https://files.phpmyadmin.net/phpMyAdmin/4.8.5/phpMyAdmin-4.8.5-all-languages.tar.gz
sudo tar zxf /tmp/phpMyAdmin.tar.gz
sudo mv phpMyAdmin-4.8.5-all-languages phpMyAdmin
cd phpMyAdmin
sudo cp config.sample.inc.php config.inc.php
sudo sed -i '/AllowNoPassword/s/false/true/' config.inc.php
sudo sed -i $'/blowfish_secret/s/\'\'/\''$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)$'\'/' config.inc.php

cd ~/environment

sudo chkconfig mysqld on
sudo chkconfig httpd on

sudo service mysqld start
sudo service httpd start
